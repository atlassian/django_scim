import pytest
from django.db import connection


# Extends the pytest-django `db` fixture to install the Postgres pgcrypto
# extension before tests marked with `django_db` are run.
#
# https://github.com/pytest-dev/pytest-django/blob/master/pytest_django/fixtures.py#L141

@pytest.fixture(scope='function')
def db(db):
    with connection.cursor() as cursor:
        cursor.execute('CREATE EXTENSION IF NOT EXISTS pgcrypto')
