import pytest

from django.contrib.auth.models import User
from django_scim.models import SCIMUser
from pytz import utc

from .test_utils import _make_django_user


@pytest.mark.django_db
def test_scim_user_model_emails():
    """Assert that the model does the correct things"""
    django_user = _make_django_user()
    scim_user = SCIMUser(User.objects.get(pk=django_user.pk))

    assert scim_user.emails == {u'jeffgravy@yahoo.com': True}


@pytest.mark.django_db
def test_scim_user_model_to_dict():
    """Assert that the model does the correct things"""
    django_user = _make_django_user()
    scim_user = SCIMUser(User.objects.get(pk=django_user.pk))

    scim_user_dict = {
        'schemas': ['urn:scim:schemas:core:1.0'],
        'id': str(django_user.id),
        'userName': django_user.username,
        'name': {
            'formatted': scim_user.display_name,
            'familyName': django_user.last_name,
            'givenName': django_user.first_name,
        },
        'displayName': scim_user.display_name,
        'emails': [{'value': email, 'primary': primary}
                   for email, primary in scim_user.emails.items()],
        'active': django_user.is_active,
        'groups': [],
        'meta': {
            'created': utc.localize(django_user.date_joined).isoformat(),
            'lastModified': utc.localize(django_user.date_joined).isoformat(),
            'location': u'/api/internal/scim/v2/Users/{}'.format(django_user.pk)
        }
    }
    assert scim_user.to_dict() == scim_user_dict


@pytest.mark.django_db
def test_scim_user_model_display_name():
    django_user = _make_django_user()
    scim_user = SCIMUser(User.objects.get(pk=django_user.pk))

    assert scim_user.display_name == 'Jeff Gravy'
