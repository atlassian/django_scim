from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User


def _make_django_user(password_hasher='bcrypt_sha256', **kwargs):
    password = kwargs.pop('password', 'biscuits')
    params = {'username': 'jeffgravy',
              'email': 'jeffgravy@yahoo.com',
              'first_name': 'Jeff',
              'last_name': 'Gravy'}
    params.update(kwargs)

    return User.objects.create(
        password=make_password(password, hasher=password_hasher), **params)
